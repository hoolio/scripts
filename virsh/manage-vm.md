manage-vm
=========

Perl script to manage kvm vms via virsh
```
$ ./manage.vm.sh
This command needs to be run as root (use sudo)

$ sudo ./manage.vm.sh
usage: manage.vm.sh list: List all guests on the current host
usage: manage.vm.sh backup <vmname>: Shutdown, backup & restart a guest
usage: manage.vm.sh migrate <vmname> <destination host>: Shutdown, copy, define remote, destroy local & startup remote

$ sudo ./manage.vm.sh migrate ansible feathertail
Considering migrating ansible to feathertail
 ..testing to see if we have the correct permissions on the remote host
 ..yay! connectivity with remote host established
Parsing KVM configuration for ansible
 ..assuming config file is at /etc/libvirt/qemu/ansible.xml
 ..attempting to determine datafile location
 ..ansible is configured to use /var/lib/libvirt/images/ansible/tmpfyi1SJ.qcow2
Checking to see if the guest is running
  !!WARNING   guest is still running, initiating shutdown.  Is an outage OK? .. ..enter L8 to verify:  L8
 ..acknowledging verification commencified
 ..shutting down, please wait  :) :) :) ansible now not running

Are you sure you want to proceed migrating ansible to feathertail? ..enter Q2 to verify:  ^C

$ sudo virsh start ansible
Domain ansible started
```
