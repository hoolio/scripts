#!/usr/bin/perl
#
# Copyright 2015 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# prepares local HDDs for use as swift storage node
#
# assuming host has 13 disks, and root, efi and swap are all on sda.
#  and that sdb through sdm are unformatted and unmounted
#
# format each disk N in disks[] with label N in labels[]
#   ie mkfs.xfs -L store12 /dev/sdm
#
# the final layout we are looking for is;
# lsblk
# NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
# sda      8:0    0 278.9G  0 disk
# ├─sda1   8:1    0   512M  0 part /boot/efi
# ├─sda2   8:2    0 214.4G  0 part /
# └─sda3   8:3    0    64G  0 part [SWAP]
# sdb      8:16   0   1.8T  0 disk /srv/node/store01
# sdc      8:32   0   1.8T  0 disk /srv/node/store02
# sdd      8:48   0   1.8T  0 disk /srv/node/store03
# sde      8:64   0   1.8T  0 disk /srv/node/store04
# sdf      8:80   0   1.8T  0 disk /srv/node/store05
# sdg      8:96   0   1.8T  0 disk /srv/node/store06
# sdh      8:112  0   1.8T  0 disk /srv/node/store07
# sdi      8:128  0   1.8T  0 disk /srv/node/store08
# sdj      8:144  0   1.8T  0 disk /srv/node/store09
# sdk      8:160  0   1.8T  0 disk /srv/node/store10
# sdl      8:176  0   1.8T  0 disk /srv/node/store11
# sdm      8:192  0   1.8T  0 disk /srv/node/store12

use strict;
use warnings;

# make sure we have /sbin/mkfs.xfs installed
print ".. checking for /sbin/mkfs.xfs";
if (not -e "/sbin/mkfs.xfs") { print "\nERROR; unable to find /sbin/mkfs.xfs\n"; exit;}
print "  OK\n";

# declare arrays of disks lables and disk ids.
my @disks=('/dev/sdb','/dev/sdc','/dev/sdd','/dev/sde','/dev/sdf','/dev/sdg','/dev/sdh','/dev/sdi','/dev/sdj','/dev/sdk','/dev/sdl','/dev/sdm');
my @labels=('store01','store02','store03','store04','store05','store06','store07','store08','store09','store10','store11','store12');

# count the actual physical disks
my $return=`lsscsi | wc -l`; chomp($return);
my $total_number_of_pysical_disks = $return;

# check to see if there are enough
print ".. do we have 13 logical disks?";
if ($total_number_of_pysical_disks != 13) { print "\nERROR; $total_number_of_pysical_disks != 13\n"; exit; }
print "  OK\n";

# make sure that the only mounted disk is sda
# first count all the sd mounted disks
my $sd_mounts = `mount | grep sd | wc -l`;
# then count the sda mounts
my $sda_mounts = `mount | grep sda | wc -l`;

print ".. checking to see that sda and only sda is mounted";
if ($sd_mounts != $sda_mounts) { print "ERROR: sd_mounts not the same as sda_mounts"; exit; }
if ($sda_mounts !=2) { print "ERROR: sda_mounts != 2"; exit; }
print "  OK\n\n";

# parse through the disks and format and label them
for (my $i = 0; $i < @labels; $i++) {
        # format the disks
        my $cmd= "mkfs.xfs -f -L $labels[$i] $disks[$i]";
        print ".. running the command: '$cmd'\n";
        my $return = `$cmd`;

        # create mount points
        my $cmd2 = "mkdir -p /srv/node/$labels[$i]";
        print ".. running the command: '$cmd2'\n";
        my $return2 = `$cmd2`;
}
