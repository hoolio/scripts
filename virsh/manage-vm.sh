#!/usr/bin/perl
#use strict;

# ## ABOUT ##
# see sub_show_usage();

# ## DEPENDENCIES ##
# x) File::Grep (sudo cpan install File::Grep)
# x) use String::Random (sudo apt-get install libstring-random-perl)
#
# ## NOTES ##
# x) naming can vary between actual vm name, vm data file, and hostname; we rely on vmname and the rest is figured out.
# 
# ## BUGS!! ##
# x) we're not trapping ANY return codes etc from rsync or virsh etc == BAD.

# evaluate the number of arguments passed
$num_args=$#ARGV + 1;

# include the File::Grep module
use File::Grep qw( fgrep fmap fdo );
use String::Random;
use File::Basename;

# ensure this is being run as root
$whoami = getpwuid($>);
if ($whoami ne "root") {
	print "This command needs to be run as root (use sudo)\n";
	exit;
}

#print "Starting operation at ".scalar localtime()."\n";
use Switch;

switch ($ARGV[0]) {
	case list { sub_show_vm_list(); }
	case backup { sub_backup_vm($ARGV[1]); }
	case migrate { sub_migrate_vm($ARGV[1],$ARGV[2]); }
	else { sub_show_usage(); }
}

#print "Ending operation at ".scalar localtime()."\n";

sub sub_show_usage() {
	print "usage: manage.vm.sh list: List all guests on the current host\n";
    print "usage: manage.vm.sh backup <vmname>: Shutdown, backup & restart a guest\n";
    print "usage: manage.vm.sh migrate <vmname> <destination host>: Shutdown, copy, define remote, destroy local & startup remote\n";
    exit;
}

sub sub_test_remote_permissions($) {
	local($destination_host=$_[0]);		
	$return=`ssh -o PasswordAuthentication=no $destination_host hostname`;
	@returnsplit = split (/\s+/,$return);
	#print @returnsplit[0];
	if (@returnsplit[0] eq $destination_host) {
		print " ..yay! connectivity with remote host established\n";
		return "OK";
	} else {
		print "\n!!!ERROR   Unable to talk to remote host, need passwordless ssh root certificates; see a nerd!\n";
		exit;
	}	
}

sub sub_migrate_vm($ $) {
	local($guest_name=$_[0]);
	local($destination_host=$_[1]);
    my $date=`date +%Y.%m.%d_%H%M`; chomp($date);
	
	# ensure that the user has entered a guest 
	if (not defined($guest_name)) { print "\n!!!ERROR: No guest name entered, exiting\n\n"; sub_show_usage(); exit; }
	# ensure that this guest exists on this kvm host
	if (sub_verify_vm_name($guest_name) ne "verified") { print "\n!!!ERROR:   $guest_name not found on this host\n"; exit; }
	# ensure that the user has entered a destination_host
	if (not defined($destination_host)) { print "ERROR: No remote host name entered, exiting\n\n"; sub_show_usage(); exit; }
		
	print "Considering migrating VM $guest_name to HOST $destination_host\n";
	
	print " ..testing to see if we have the correct permissions on the remote host\n";
	if (sub_test_remote_permissions($destination_host) ne "OK") { print "Permissions error!\n"; exit; }
			
	# Gather some data about the vm we wish to migrate
	my $xmlfile="/etc/libvirt/qemu/$guest_name.xml";	
	#print " ..okay, we're assuming the config file is $xmlfile\n";
	#print " ..found $xmlfile\n";
	@DataFiles = sub_parse_datafile_location($guest_name);
    
    # ensure that it all looks sane before proceeding
    print "Does all of that look sensible to you?"; 	
    sub_verify_to_proceed();
    
	# determine if the guest is running and handle it
	print "Checking to see if the guest is running\n";
	if (sub_determine_current_status($guest_name) eq "running") {
		print "  !!WARNING   guest is still running, initiating shutdown.  Is an outage OK? ..";
		sub_shutdown_vm($guest_name);
	} else {
		print " ..determined that $guest_name is not running\n";
	}
    
    # confirm then actually do the migration
    print "\nAre you sure you want to proceed migrating $guest_name to $destination_host?"; 
    sub_verify_to_proceed();
    
    print "OK, Copying machine definintion and datafiles to remote host\n";
    # copy the datafile over
	foreach (@DataFiles) {
		$data_file = $_;
        
        # make sure the remote path exists
        my($filename, $directories, $suffix) = fileparse($data_file);
        my $results=`ssh $destination_host mkdir -p $directories`;
                
        # copy the datafile(s)
        $remote_data_file = $destination_host.":".$data_file;
        sub_copy_file($data_file,$remote_data_file);
        
        #backup local file to .bak
        print "Renaming local datafile(s) to prevent confusion\n";        
        $data_file_old=$data_file.".movedto".$destination_host.$date.".bak";
        my $command="mv $data_file $data_file_old";
        print " ..running '$command'\n";
        my $results=`$command`;  
	}
    
    # copy the config file over
    $remote_xmlfile=$destination_host.":".$xmlfile;
    sub_copy_file($xmlfile,$remote_xmlfile);
    
    # make a local copy of the xml file just for safety.
    $backup_xmlfile=$xmlfile.".movedto".$destination_host.$date.".bak";
    sub_copy_file($xmlfile,$backup_xmlfile);
    
    # undefine the local vm    
    print "Undefining $guest_name on localhost\n";    
    my $command="virsh undefine $guest_name";
    print " ..running '$command'\n";
    my $results=`$command`;     
    
    #define remote vm
    print "Define and start $guest_name on $destination_host\n";    
    my $command="ssh $destination_host virsh define $xmlfile";
    print " ..running '$command'\n";
    my $results=`$command`;    
    
    # start remote vm
    my $command="ssh $destination_host virsh start $guest_name";
    print " ..running '$command'\n";
    my $results=`$command`;          
}

sub sub_copy_file($ $) {    
    local($source=$_[0]);
	local($destination=$_[1]);
    my $rsync_switches="-av";
    
	my $rsync_command ="rsync $rsync_switches $source $destination";
	print " ..running $rsync_command\n";
    my $results=`$rsync_command`;
    ##print "   ^^^^ NOTE: errors from this aren't being trapped!\n";
}

sub sub_verify_to_proceed() {
	use String::Random;
	$foo = new String::Random;
	#$random_string = $foo->randpattern("CcCnCn");
	$random_string = $foo->randpattern("Cn");
	#$random_string=ASDFASD;
	
	print " ..enter $random_string to verify:  ";
	chomp($user_input = <STDIN>);
	if ($user_input eq $random_string) {
		#print "Verification passed\n";
		return "verified";
	} else {
		print "\nERROR: Verification failed, exiting\n";
		exit;		
	}
}

sub sub_backup_vm($) {
	local($guest_name=$_[0]);
	# ensure that the user has entered a guest name
	if (not defined($guest_name)) { print "ERROR: No guest name entered, exiting\n\n"; sub_show_usage(); exit; }
	# ensure that this guest exists on this kvm host
	if (sub_verify_vm_name($guest_name) ne "verified") { print "ERROR: $guest_name not found on this host\n"; exit; }
	
	# we should be right to go now, the user entered a guest and it exists on this host
	#print "$guest_name exists etc, now thinking about backing it up \n";
	
	# determine which datafiles are used by this guest
	@DataFiles = sub_parse_datafile_location($guest_name);
	
	# check to see if a backup file exists already
	foreach (@DataFiles) {
		$data_file = $_;
		$data_file_backup = $_.".backup";
		print " ..checking if backup exists\n";
		
		if (-e $data_file_backup) {
			print "WARNING!!    $data_file_backup exists.  Renaming to *.temp until we finish: ";
			if (sub_verify_to_proceed() eq "verified") {
				print " ..acknowledging verification commencified\n";
				`mv $data_file_backup $data_file_backup.temp`;
				print " ..renamed to $data_file_backup.temp\n";
			}			
		} else {
				print " ..$data_file_backup does not exist, OK.\n";
		}
	}
	
	# determine if the guest is running and handle it
	print "Checking to see if the guest is running\n";
	if (sub_determine_current_status($guest_name) eq "running") {
		print "!!WARNING   guest is still running, initiating shutdown.  Is an outage OK? ..";
		sub_shutdown_vm($guest_name);
	} else {
		print " ..determined that $guest_name is not running\n";
	}	
	
	# now ready to perform the backup
	print "Now ready to perform the backup\n";
	foreach (@DataFiles) {
		$data_file = $_;
		$data_file_backup = $_.".backup";
		sub_copy_file($data_file, $data_file_backup);
	}
	
	# now ready to restart vm
	print "\nNow ready to restart guest\n";
	sub_start_vm($guest_name);
	
	# what to do with the .temp we made, if we did so?
	print "Tidying up\n";
	foreach (@DataFiles) {
		$data_file = $_;
		$data_file_backup = $_.".backup";
		$data_file_backup_temp = $data_file_backup.".temp";
		print " ..checking if any .temp files exist\n";
		
		if (-e $data_file_backup_temp) {
			print "WARNING!!    data_file_backup_temp exists.  Comparing to the new one\n";
			print "old: ".`stat --format='%n %s %z' $data_file_backup_temp`;
			print "new:".`stat --format='%n %s %z' $data_file_backup`;
			
			print " ..should we delete the old file? ";
			if (sub_verify_to_proceed() eq "verified") {
				print " ..acknowledging verification commencified\n";
				`rm $data_file_backup_temp`;
				print " ..deleted $data_file_backup.temp\n";
			}			
		} else {
				print " ..$data_file_backup_temp does not exist, OK.\n";
		}
	}
	
	print "Finishing\n";
	print "  ..we created $data_file_backup\n";
}

sub sub_start_vm($) {
	local($guest_name=$_[0]);
	print " ..starting vm, please wait  ";
	    
	`virsh start $guest_name`;
	        
	$startup_wait_time=60;
	$startup_wait_count=0;

	while (sub_determine_current_status($guest_name) ne "running" ) {
		print ":) ";
		sleep 1;
		$startup_wait_count=$startup_wait_count+1;
		if ($shutdown_wait_count > $shutdown_wait_time) {
			print "\n\nWARNING!!  We have waited $shutdown_wait_time seconds but $guest_name isn't stopping; seek nerd help!\n";
			exit;
		}
	}
	print "$guest_name now running\n";
}

sub sub_parse_datafile_location($) {
	local($guest_name=$_[0]);
	print "Parsing KVM configuration for $guest_name\n";
	 
	my $xmlfile="/etc/libvirt/qemu/$guest_name.xml";	
	print " ..assuming config file is at $xmlfile\n";
 
    # determine exactly where the datafile is
	print " ..attempting to determine datafile location\n";
 
	if ($datafile = fgrep { /source file/ } "$xmlfile") {
		open FILE,"$xmlfile" or die $!;
	    @FileContents=<FILE>;close FILE;
 
		# Scan the file contents for our source file and put them into an array
	    @SourceFileLine=grep /source file/,@FileContents;
		foreach (@SourceFileLine){
			my @SourceFileLinesplit = split (/'/, $_);            
            if ($SourceFileLinesplit[1] =~ /iso/i){
                print " ..excluding $SourceFileLinesplit[1]\n";
            } else {
                push(@DataFiles, $SourceFileLinesplit[1]);
            }
		}
			 
		# Print the DataFiles wot we found	
		foreach (@DataFiles) { 	print " ..$guest_name is configured to use ".$_."\n" }
	} else { print "ERROR!!    Unable to locate datafile, seek nerd help!\n"; }
		
	# We now have an array @DataFiles containing the data files for this vm.
	return @DataFiles;
}

sub sub_shutdown_vm($) {
	local($guest_name=$_[0]);
	if (sub_verify_to_proceed() eq "verified") {
		print " ..acknowledging verification commencified\n";
		print " ..shutting down, please wait  ";
	    
	    `virsh shutdown $guest_name`;
	        
		$shutdown_wait_time=60;
		$shutdown_wait_count=0;

		while (sub_determine_current_status($guest_name) eq "running" ) {
			print ":) ";
			sleep 1;
			$shutdown_wait_count=$shutdown_wait_count+1;
			if ($shutdown_wait_count > $shutdown_wait_time) {
				print "\n\nERROR!!  We have waited $shutdown_wait_time seconds but $guest_name isn't stopping; seek nerd help!\n";
				exit;
			}
		}
		
		print "$guest_name now not running\n";
	} #else { print "Some logical error in sub_shutdown_vm\n"; }
}

sub sub_verify_vm_name($) {
	local($potential_guest_name=$_[0]);
	#if ($potential_guest_name eq "exists") {return "verified"; }
	$return=`virsh list --all | grep $potential_guest_name`;
	@returnsplit = split (/\s+/,$return);
	#print @returnsplit[2]."\n";
	if (@returnsplit[2] eq $potential_guest_name) { return "verified"; }
}

sub sub_determine_current_status($) {
	local($guest_name=$_[0]);
	$return=`virsh list --all | grep $guest_name`;
	@returnsplit = split (/\s+/,$return);
	#print @returnsplit[2]."\n";
	return @returnsplit[3];
}

sub sub_show_vm_list() {
	print "Outputting the current virtual machines:\n";
	system("virsh list --all");
	exit;
}
