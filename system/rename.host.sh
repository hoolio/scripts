#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# renames localhost to whatever is passed as arg0.
#
# that requires several things to happen
# X) replace instances of oldname with newname in /etc/hosts
# X) replace instances of oldname with newname in /etc/hostname
# X) execute hostname with newname
# x) run dhcpclient to update newname with any internal dynamic dns

use 5.010; 
use strict;
use warnings;

## PREAMBLE AND SETUP ##
#
# ensure this is being run as root
my $whoami = getpwuid($>);
if ($whoami ne "root") { print "This command needs to be run as root (use sudo)\n"; exit; }

# parse arguments, make sure we have enough
if (($#ARGV + 1) < 1) { print "ERROR; usage ./rename.host.sh newhostname\n"; exit 2;}

# grab the newname from the arg0
my $newname = "$ARGV[0]\n"; chomp($newname);

my $oldname=`cat /etc/hostname`; chomp($oldname);

print "oldname is $oldname\n";
print "newname is $newname\n";

## DO THE THING
# 
# personally i think this is far too low level, where's my in place search and replace Larry?
#
# move /etc/hosts to /etc/hosts.original
`mv /etc/hosts /etc/hosts.original`;

# create a filehandle for a new version of /etc/hosts
open(NEWFILE, ">>/etc/hosts") or die "Can't create /etc/hosts : $!\n";

# create a filehandle for the original version.
open(OLDFILE, "/etc/hosts.original") or die "Can’t open /etc/hosts.original : $!\n"; 

# iterate through the OLDFILE, search for strings and write out to NEWFILE
while(my $line = <OLDFILE>){ 
    chomp ($line); 

    ### replace oldname for newname in any line
    #print "Before substituting: ", $line ,"\n";
    #$line =~ s/$oldname/$newname/g;     
    $line =~ s/\b${oldname}\b/${newname}/g;
    print NEWFILE $line,"\n";
    #print "After substituting : ", $line ,"\n\n";
} 

# close file handles
close(NEWFILE);
close(OLDFILE);

# run `hostname` to have the new hostname take effect.
`hostname $newname`;

# write out a new /etc/hostname
`echo $newname > /etc/hostname`;

# renew ip address to update hostname in dynamic dns
`dhclient`;

print "New bash instances will reflect hostname change\n";
