#!/usr/bin/python
#
# dircomp.py 0.1
# Copyright Julius Roberts 2017
# Released under the terms of the GPLv3 (https://www.gnu.org/licenses/gpl-3.0-standalone.html)
#
# see https://github.com/hooliowobbits/scripts/blob/master/system/dircomp/README.md

import os
import hashlib
import argparse
import sys
import string
import itertools
import shelve
import random

helpText="Determine if the two supplied paths are mutual duplicates by performing\
            increasingly expensive tests.  Any failure aborts remaining tests."

parser = argparse.ArgumentParser(description=helpText)

parser.add_argument("path1", help="the path to compare to path2")
parser.add_argument("path2", help="the path to compare to path1")
parser.add_argument("-hd","--hashdepth", help="percentage of file pairs to hash (where < maxsize)", type=int, default=1)
parser.add_argument("-ms","--maxsize", help="maximim size of a file to hash in MB", type=int, default=16)
parser.add_argument("-ns","--nostat", help="don't stat files, less accuracy but faster.",default=False, action='store_true')
parser.add_argument("-s","--show", help="show n percentage of database elements",type=int, default=0)
parser.add_argument("-o","--options", help="display the configured options",default=False, action='store_true')
parser.add_argument('--version', action='version', version='%(prog)s 0.1, Julius Roberts 2017')
#parser.add_argument("-v", help="increase verbosity", action="store_true")

args = parser.parse_args()

# MAKE SURE SPECIFIED PATHS EXIST
paths = (args.path1.rstrip("/"), args.path2.rstrip("/")) #trim any trailing slashes of path
for path in paths:
    if not os.path.exists(path):
        print "ERROR: %s does not exist!" % path
        sys.exit()

def produceHashOfFile(path):
    #with open(path, 'rb') as thing:
    with open(path, 'r') as fileToHash:
        hasher = hashlib.md5()
        hasher.update(fileToHash.read())
        hashKey = hasher.hexdigest()
    return hashKey

def determineNumberOfDatabaseLines(filedatabasepath):
    s = shelve.open(filedatabasepath)
    length = len(s)
    s.close()
    return length

def produceFileListing(path, filedatabasepath):
    # sometimes the filedatabasepath will not have been deleted (crash?)
    if os.path.exists(filedatabasepath): os.remove(filedatabasepath)

    pathToWalk = os.walk(path)
    s = shelve.open(filedatabasepath,writeback=True)
    #s = shelve.open(filedatabasepath)

    for dirpath, subdirs, files in pathToWalk:
        for idx, f in enumerate(files):
            name = f # libsasl2-modules
            fullpath = os.path.join(dirpath, name) #
            relativepath = fullpath.replace(path,"") #make path relative
            rootpath = path

            # print "f:           %s" % f
            # print "path:        %s" % path
            # print "fullpath     %s" % fullpath
            # print "relativepath %s" % relativepath

            if not os.path.islink(fullpath): # exclude symlinks
                if args.nostat: # check if stat of files is disabled
                    s['%i' % idx] = { 'rootpath':rootpath, 'name': name, 'fullpath': fullpath, 'relativepath':relativepath }
                else:
                    fileinfo = os.stat(fullpath) # stat file to grab details
                    size = fileinfo.st_size
                    s['%i' % idx] = { 'rootpath':rootpath, 'name': name, 'fullpath': fullpath, 'size': size, 'relativepath':relativepath }
                    #s['%i' % idx] = { 'name': name, 'size': size }

    #s.sync()
    s.close()

def readFileListingDatabase(elementid,filedatabasepath):
    s = shelve.open(filedatabasepath)
    return s['%i' % elementid]
    s.close()

def cleanUp(filedatabaselist):
    print "\nDeleting databases:"
    for filedatabasepath in filedatabaselist:
        if os.path.exists(filedatabasepath + ".db"):
            print "..deleting %s.db" % filedatabasepath
            os.remove(filedatabasepath + ".db")
    sys.exit()

def main():
    if args.options:
        #print "dircomp.py v0.1   Julius Roberts 2017"
        print "Specified Options:"
        print "..path1:    %s" % args.path1
        print "..path2:    %s" % args.path2
        print "..hashdepth %i%% " % args.hashdepth
        print "..maxsize:  %iMB" % args.maxsize
        print "..nostat:   %i" % args.nostat
        print "..show:     %i%%" % args.show
        print

    # ***** PRODUCE FILE LIST DATABASES *****
    # ---------------------------------------
    print "Producing file listing databases:"
    filedatabaselist = list()
    hashesoffiledatabases = list()

    for idx, path in enumerate(paths):
        filedatabasepath = "path" + str(idx+1) + "_shelve"
        filedatabaselist.append(filedatabasepath)
        print "..walking path %s into %s " % (path, filedatabasepath)
        produceFileListing(path,filedatabasepath)

    if args.show > 0:
        print "\nReading back %i%% of databases:" % args.show
        for filedatabasepath in filedatabaselist:
            print "..found %s" % filedatabasepath
            numberOflines = determineNumberOfDatabaseLines(filedatabasepath)
            numberOfLinesToShow = int((args.show * numberOflines ) / 100)
            for idx in range(0, numberOfLinesToShow+1):
                line = readFileListingDatabase(idx,filedatabasepath)
                print "   %i: %s" % (idx,line)

    # ***** COMPARE BY DATABASE LENGTH *****
    # --------------------------------------
    print "\nComparing databases (by length):"
    blah = list()
    for filedatabasepath in filedatabaselist: blah.append(determineNumberOfDatabaseLines(filedatabasepath))
    lengthOfPath1Database = blah[0]
    lengthOfPath2Database = blah[1]
    if lengthOfPath1Database == lengthOfPath2Database:
        print "..databases match on length (%i)" % lengthOfPath1Database
    else:
        print "\nFAIL databases do not match on length (%i vs %i)" % (lengthOfPath1Database,lengthOfPath2Database)
        cleanUp(filedatabaselist)

    # ***** COMPARE BY ELEMENT *****
    # ------------------------------
    print "\nComparing file listing databases (by element):"
    # we can assume now that lengthOfPath1Database == lengthOfPath2Database
    for idx in range(0, lengthOfPath1Database-1):
        # here we read a specifed line (idx) from each database
        blah = list()
        for filedatabasepath in filedatabaselist:
            blah.append(readFileListingDatabase(idx,filedatabasepath))
        lineFromPath1 = blah[0]
        lineFromPath2 = blah[1]
        # so we now have two specific lines which have the same key (idx)
        # now we can compare the two lines on their elements 'relativepath' and 'size'
        # other elements are not compared because they're path specific

        # we just need to check if we can compare based on size.  if nostat is 1 then the databases
        # won't have the size attribute.  this section is ugly.
        fail = 0
        if args.nostat == 1: # ie stat is disabled, we can't compare on
            if (lineFromPath1['relativepath']) == (lineFromPath2['relativepath']):
                pass
                #print "..line %i matches OK (%s)" % (idx,lineFromPath1['absolutefullname'])
            else: fail = 1
        else: # ie stat is ok, we can compare on size
            if (lineFromPath1['relativepath'],lineFromPath1['size']) == (lineFromPath2['relativepath'],lineFromPath1['size']):
                pass
                #print "..line %i matches OK (%s)" % (idx,lineFromPath1['absolutefullname'])
            else: fail = 1

        if fail == 0:
            pass
            #print "..line %i matches OK (%s)" % (idx,lineFromPath1['absolutefullname'])
        else:
            print "..line %i from path1: %s " % (idx,lineFromPath1)
            print "..line %i from path2: %s " % (idx,lineFromPath2)
            print "\nFAIL Databases do not match on line %i" % idx
            cleanUp(filedatabaselist)

    print "..all %i database elements match" % lengthOfPath1Database

    # ***** COMPARE BY HASHING FILES *****
    # ------------------------------------
    numberOfFilesInDatabase = determineNumberOfDatabaseLines(filedatabasepath)
    numberOfFilesToHash = numberOfFilesInDatabase * args.hashdepth / 100
    if numberOfFilesToHash == 0: numberOfFilesToHash = 1
    fileSizeLimitInBytes = args.maxsize * 1024 * 1024
    print "\nHashing %i%% of file pairs (ie %i):" % (args.hashdepth,numberOfFilesToHash)

    count = 0
    while count < numberOfFilesToHash:

        blah = list()
        randint = random.randint(1,numberOfFilesInDatabase-1)
        for filedatabasepath in filedatabaselist:
            blah.append(readFileListingDatabase(randint,filedatabasepath))

        lineFromPath1 = blah[0]
        lineFromPath2 = blah[1]
        hashOflineFromPath1 = produceHashOfFile(lineFromPath1['fullpath'])
        hashOflineFromPath2 = produceHashOfFile(lineFromPath2['fullpath'])

        if args.nostat == 0:
            print ".. %s %s (%i bytes of %i max)" % (hashOflineFromPath1,lineFromPath1['fullpath'],lineFromPath1['size'],fileSizeLimitInBytes)
            print ".. %s %s (%i bytes of %i max)" % (hashOflineFromPath2,lineFromPath2['fullpath'],lineFromPath1['size'],fileSizeLimitInBytes)

            if (lineFromPath1['size'] > fileSizeLimitInBytes) or (lineFromPath2['size'] > fileSizeLimitInBytes):
                next
        else:
            print ".. %s %s" % (hashOflineFromPath1,lineFromPath1['fullpath'])
            print ".. %s %s" % (hashOflineFromPath2,lineFromPath2['fullpath'])

        count = count + 1

        if not hashOflineFromPath1 == hashOflineFromPath2:
            print "%s %s" % (hashOflineFromPath1,lineFromPath1['fullpath'])
            print "%s %s" % (hashOflineFromPath2,lineFromPath2['fullpath'])
            print "\nFAIL Hashes not match!"
            cleanUp(filedatabaselist)

    print "..all hash values match!"

    print "\nOK! Using the specified options these paths are identical:"
    for idx, path in enumerate(paths):
        print "  %s" % (path)

    cleanUp(filedatabaselist)

main()
