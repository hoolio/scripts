dircomp.py 0.1
Copyright Julius Roberts 2017
Released under the terms of the GPLv3 (https://www.gnu.org/licenses/gpl-3.0-standalone.html)

Determine if the two supplied paths are mutual duplicates by performing
increasingly expensive tests.  Any failure aborts remaining tests.

THE PREMISE:
* we have a very large POSIX filesystem
* users have copied directory trees around wasting space
* we think we know what those trees are (path1 and path2)
* doing a hash of everything in both trees is expensive
* we want to identify two trees as the same, as cheaply as practical
* we consider the following as sufficient evidence that the trees match:
1. relative paths and sizes (permissions may vary)
1. AND file pairs selected randomly have matching hashes
* we can improve confidence by increasing the number of file pairs selected as a % "hashdepth"
* the preferred hashing algorithm favours speed over cryptographical security

HOW THIS WORKS:
* accepts args path1 and path2
* produces two shelve databases of file listings of path1 and path2
* test1 being do these databases have the same number of entries?
* test2 compare each database entry (relative path and size (if not nostat))
* test3 hash a specified percentage (hashdepth) of file pairs

NOTES:
* should be computationally cheap
* should prefer files over memory
* should abort as soon as difference detected
* should tidy up after itself
* should use python 2.7 and minimal dependencies

Help text:
```
$ ./dircomp.py -h
usage: dircomp.py [-h] [-hd HASHDEPTH] [-ms MAXSIZE] [-ns] [-s SHOW] [-o]
                  [--version]
                  path1 path2

Determine if the two supplied paths are mutual duplicates by performing
increasingly expensive tests.  Any failure aborts remaining tests.

positional arguments:
  path1                 the path to compare to path2
  path2                 the path to compare to path1

optional arguments:
  -h, --help            show this help message and exit
  -hd HASHDEPTH, --hashdepth HASHDEPTH
                        percentage of file pairs to hash (where < maxsize)
  -ms MAXSIZE, --maxsize MAXSIZE
                        maximim size of a file to hash in MB
  -ns, --nostat         don't stat files, less accuracy but faster.
  -s SHOW, --show SHOW  show n percentage of database elements
  -o, --options         display the configured options
  --version             show program's version number and exit
```

Compare to non existent directory:
```
$ ./dircomp.py /blah /blah2
ERROR: /blah does not exist!
```

Compare two different directories:
```
$ ./dircomp.py /etc/ /var/
Producing file listing databases:
..walking path /etc into path1_shelve.db
..walking path /var into path2_shelve.db

Comparing databases (by length):

FAIL databases do not match on length (94 vs 3358)
```

Compare two identical directories:
```
$ ./dircomp.py /etc/ ./etc/
Producing file listing databases:
..walking path /etc into path1_shelve.db
..walking path ./etc into path2_shelve.db

Comparing databases (by length):
..databases match on length (94)

Comparing file listing databases (by element):
..all 94 database elements match

Hashing 1% of file pairs (ie 1):
.. d3fba0b07fc3767e0c7b13d11adefd3b /etc/init.d/.depend.stop (825 bytes of 16777216 max)
.. d3fba0b07fc3767e0c7b13d11adefd3b ./etc/init.d/.depend.stop (825 bytes of 16777216 max)
..all hash values match!

OK! Using the specified options these paths are identical:
  /etc
  ./etc
```

Compare two identical directories, show output, disable stat (size)
and hash files less than 1mb to a total of 10% of all files.
```
$ ./dircomp.py -o -ns -hd 10 -ms 1 /etc/ ./etc/
Specified Options:
..path1:    /etc/
..path2:    ./etc/
..hashdepth 10%
..maxsize:  1MB
..nostat:   1
..show:     0%

Producing file listing databases:
..walking path /etc into path1_shelve.db
..walking path ./etc into path2_shelve.db

Comparing databases (by length):
..databases match on length (94)

Comparing file listing databases (by element):
..all 94 database elements match

Hashing 10% of file pairs (ie 9):
.. 068230f47d2dd6782ce28051c3eb6251 /etc/init.d/cloud-config
.. 068230f47d2dd6782ce28051c3eb6251 ./etc/init.d/cloud-config
.. 559387f792462a62e3efb1d573e38d11 /etc/cron.daily/bsdmainutils
.. 559387f792462a62e3efb1d573e38d11 ./etc/cron.daily/bsdmainutils
.. e5e12910bf011222160404d7bdb824f2 /etc/cron.daily/.placeholder
.. e5e12910bf011222160404d7bdb824f2 ./etc/cron.daily/.placeholder
.. b5ca46c0c11ace2a1e40217c29646bf2 /etc/init.d/hwclock.sh
.. b5ca46c0c11ace2a1e40217c29646bf2 ./etc/init.d/hwclock.sh
.. e467745743809f974051c3ec5b9cd022 /etc/cron.daily/man-db
.. e467745743809f974051c3ec5b9cd022 ./etc/cron.daily/man-db
.. 05d217072d397076fb17c42d27d66564 /etc/init.d/acpid
.. 05d217072d397076fb17c42d27d66564 ./etc/init.d/acpid
.. 75858dffc595a0ec73c46a1e5744d087 /etc/inputrc
.. 75858dffc595a0ec73c46a1e5744d087 ./etc/inputrc
.. 5edc6a396492e3c3f39bb4d05932d692 /etc/init.d/postfix
.. 5edc6a396492e3c3f39bb4d05932d692 ./etc/init.d/postfix
.. 8c0619be413824f1fc7698cee0f23811 /etc/debconf.conf
.. 8c0619be413824f1fc7698cee0f23811 ./etc/debconf.conf
..all hash values match!

OK! Using the specified options these paths are identical:
  /etc
  ./etc
```
