#!/usr/bin/python
#
# iotest.sh
# Julius Roberts 2017
# https://github.com/hooliowobbits/scripts/tree/master/system/iotest
#
# Test i/o by doing N write/read tests from/to /dev/zero to/from $target

import sys
import os
import subprocess
import argparse
import numpy
DEVNULL = open(os.devnull, 'wb')
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

#-----------
# ARGPARSE
#-----------
parser = argparse.ArgumentParser(description='Do N write/read tests from/to /dev/zero to/from $target',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--source', type=str, default='/dev/zero', help='Where to read data from.')
parser.add_argument('dest', type=str, help='Where to write data to.')
parser.add_argument('--count', type=int, default=4, help='Number of test iterations.')
parser.add_argument('--size', type=int, default=150, help='Payload per iteration in MB.')
parser.add_argument('--verbose', action='store_true', help='Output return from each test.')

# print help if no args given
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

#-----------
# COMMANDS
#-----------
write_test='dd if=%s of=%s/deleteme.dat bs=1M count=%s oflag=direct' % (args.source,args.dest,args.size)
read_test='dd if=%s/deleteme.dat of=/dev/null bs=1M count=%s iflag=direct' % (args.dest,args.size)

#-----------
# DEF TEST
#-----------
def perform_test(test_name,test_prettyname):
  test_command=test_name + ' 2>&1 | grep copied'
  list_of_speeds_in_MBs = []
  for x in range(0,args.count):
    p = subprocess.Popen(test_command, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL, close_fds=True)
    output = p.stdout.read()

    # handle printing of output
    if args.verbose:
      print ".." + output.rstrip('\n')
    else:
      xplus1 = x + 1
      print " %s" % xplus1,
      #print ".",
      if x == (args.count-1): print ' OK!'

    # parse output
    output = output.split()
    speed = float(output[9])
    unit = output[10]
    if unit == 'GB/s': speed = speed * 1024
    list_of_speeds_in_MBs.append(speed)

  # Calculate statistics, ping style
  #rtt min/avg/max/mdev = 0.024/0.026/0.028/0.002 ms
  average = round(sum(list_of_speeds_in_MBs) / len(list_of_speeds_in_MBs),2)
  minimum = min(list_of_speeds_in_MBs)
  maximum = max(list_of_speeds_in_MBs)
  mdev= round(numpy.std(list_of_speeds_in_MBs),2)
  print "%s: min/avg/max/mdev is %s/%s/%s/%s MB/s" % (test_prettyname,minimum,average,maximum,mdev)

#-----------
# RUN TESTS
#-----------
print "Doing %s write/read tests of %s MB from/to %s to/from %s/deleteme.dat" % (args.count,args.size,args.source,args.dest)

if args.verbose: print "writing with: %s" % write_test
perform_test(write_test,"write")

if args.verbose: print "reading with with: %s" % read_test
perform_test(read_test,"read")
