#!/usr/bin/perl -w
#
# Copyright 2016 Julius Roberts.  
# Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# runs curl against the very useful ipinfo api and;
#  - parses output into various fields for display
#  - returns ALERT iff actual ip matches predefined constant myip
#
# available fields are:
# $ curl ipinfo.io
# {
#   "ip": "168.1.23.112",
#   "hostname": "70.17.01a8.ip4.static.sl-reverse.com",
#   "city": "Sydney",
#   "region": "New South Wales",
#   "country": "AU",
#   "loc": "-33.8678,151.2073",
#   "org": "AS36351 SoftLayer Technologies Inc.",
#   "postal": "1001"
#}

use 5.010; 
use strict;
use warnings;
use JSON qw( decode_json );
use Capture::Tiny ':all';

## PREAMBLE AND SETUP ##
#
#my $myip = "141.211.155.185";

## GATHER
#
 my ($stdout, $stderr, $exit) = capture {
     system( 'curl http://ipinfo.io' );

     # These are here for LEGACY reference only, they don't return JSON and will break this
     #system( 'curl -s checkip.dyndns.org|sed -e 's/.*Current IP Address: //' -e 's/<.*$//'' );
     #system( 'wget -qO- http://ipecho.net/plain' );
     #system( '#lynx -dump https://ipleak.net/ | grep "Your IP Address" -A 5 | tail -4' );
   };
chomp ($stdout, $stderr, $exit);

## PARSE
#
my $decoded = decode_json($stdout);
my $decoded_ip = $decoded->{'ip'};
my $decoded_hostname = $decoded->{'hostname'};
my $decoded_city = $decoded->{'city'};
my $decoded_region = $decoded->{'region'};
my $decoded_country = $decoded->{'country'};
my $decoded_loc = $decoded->{'loc'};
my $decoded_org = $decoded->{'org'};
my $decoded_postal = $decoded->{'postal'};

# OUTPUT
#
# display ALERT if required
#if ($decoded_ip eq $myip) { print "\nALERT!!  YOU ARE USING AN INSECURE IP ADDRESS!!\n"; }

# output everything else
print "\n";
if ($decoded_hostname) {
  print "$decoded_ip ($decoded_hostname)\n";
} else {
  print "$decoded_ip (hostname not found)\n";
}
print "$decoded_org\n";
print "$decoded_city, $decoded_region, $decoded_country ($decoded_loc)\n";
print "\n";
